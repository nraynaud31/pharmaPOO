import Medicament from "./Medicament";

export default class Vaccin extends Medicament{
    private type: string;

    constructor(type: string, nom: string, prix: number, stock: number){
        super(nom, prix, stock);
        this.type = type;
    }

    getType(){
        return this.type;
    }
}