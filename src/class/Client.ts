import IAdresse from "../models/IAdresse";
import Medicament from "./Medicament";
import Personne from "./Personne";
import Vaccin from "./Vaccin";

export default class Client extends Personne{
    
    nom: string;
    private credit: number;
    private list_vaccins: String[] = [];
    adresse: IAdresse | undefined;

    constructor(nom: string, credit: number, list_vaccins: String[], adresse?: IAdresse | undefined) {
        super()
        this.nom = nom;
        this.credit = credit;
        this.list_vaccins = list_vaccins;
        this.adresse = adresse;
    }

    augmenterCredit(credit: number): void {
        this.credit += credit;
    }

    diminuerCredit(credit: number): void {
        this.credit -= credit;
    }

    getCredit(): number {
        return this.credit;
    }

    getNom(): string {
        return this.nom;
    }
    
    getAdresse(): IAdresse | undefined{
        return this.adresse;
    }

    getListVaccins(): String[] {
        return this.list_vaccins;
    }

    isProtected(vaccin: Vaccin) {
        for(let i in this.list_vaccins){
            if(this.list_vaccins[i] === vaccin.getNom()){
                return ;
            }
        }
    }

    acheterMedicament(medicament: Medicament) {
        if (medicament.getStock() > 0 && this.credit > medicament.getPrix()) {
            // On retire de l'argent au client
            this.diminuerCredit(medicament.getPrix());

            medicament.diminuerStock(1);
            return console.log(medicament.getNom(), this.credit, medicament.getStock());
        } else {
            return console.log("Erreur: Il n'y a plus assez de stocks pour ce médicament, ou le crédit est insuffisant !");
        }
    }

    acheterVaccin(vaccin: Vaccin) {
        if (vaccin.getStock() > 0 && this.credit > vaccin.getPrix()) {
            // On retire de l'argent au client
            this.diminuerCredit(vaccin.getPrix());

            vaccin.diminuerStock(1);
            return console.log(vaccin.getNom(), this.credit, vaccin.getStock());
        } else {
            return console.log("Erreur: Il n'y a plus assez de stocks pour ce vaccin, ou le crédit est insuffisant !");
        }
    }
}