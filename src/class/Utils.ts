import Client from "./Client";
import Medicament from "./Medicament";

export default class Utils{


    /*
    Le but d'une méthode statique est de contenir une suite d'instructions pour ne pas répéter la même chose.
    Elle ne viennent jamais changer l'état de l'algorithme. Elle se contente de prendre des paramètres en entrée,
    réaliser les traitements, et sortir un résultat.
    */
    static consoleArray(arr: Medicament[] | Client[]){
        return console.table(arr);
    }
}