export default class Medicament
{

    private nom: string;
    private prix: number;
    private stock: number;

    constructor(nom: string, prix: number, stock: number){
        this.nom = nom;
        this.prix = prix;
        this.stock = stock;
    }

    public getNom(){
        return this.nom;
    }

    public getPrix(){
        return this.prix;
    }

    public getStock(){
        return this.stock;
    }

    public augmenterStock(stock: number){
        return this.stock += stock;
    }

    public diminuerStock(stock: number){
        return this.stock -= stock;
    }

}