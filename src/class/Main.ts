import IAdresse from '../models/IAdresse';
import Client from './Client';
import Medicament from './Medicament';
import Utils from './Utils';
import Vaccin from './Vaccin';

export default class Main {
    private clients: Client[] = []; // : Client[] indique que la propriete privée clients ne peut contenir que des objets de type Client
    private medicaments: Medicament[] = []; // pareil pour les medicaments
    private vaccins: Vaccin[] = [];
    private list_vaccins: any[] = [];
    private array: any[] = [];
    private liste_vaccines: [] = [];

    ajouterClient(nom: string, credit: number, list_vaccins: String[], adresse?: IAdresse) {
        const nouveauClient: Client = new Client(nom, credit, list_vaccins, adresse);

        this.clients.push(nouveauClient);

        Utils.consoleArray(this.clients)
    }

    ajouterMedicament(nom: string, prix: number, stock: number) {
        const new_medoc: Medicament = new Medicament(nom, prix, stock);

        this.medicaments.push(new_medoc);

        Utils.consoleArray(this.medicaments)
    }

    ajouterVaccin(type: string, nom: string, prix: number, stock: number) {
        const new_vaccin: Vaccin = new Vaccin(type, nom, prix, stock);

        this.vaccins.push(new_vaccin);

        Utils.consoleArray(this.vaccins)
    }

    ajouterMaladie(nom: string, vaccin: string) {

        this.list_vaccins.push(nom, vaccin);
        this.insertionListeVaccins()
    }

    isProtected(client: string, maladie: string) {
        for (let i in this.list_vaccins) {
            console.log("vaccins de i", this.list_vaccins[i]);

            if (this.array[i] === maladie) {
                let result = this.array[1];
                console.log("result", result);

                for (let j in this.rechercherClient(client).getListVaccins()) {
                    console.log("vaccins de j", this.rechercherClient(client).getListVaccins()[j]);

                    if (this.rechercherClient(client).getListVaccins()[j] === result) {
                        return console.log("Le client est protégé de cette maladie");
                    }
                }
            }
        }
    }

    rechercherClient(nom: string) {
        return this.clients.filter(client => client.getNom() === nom)[0]
    }

    rechercherMedicament(nom: string) {
        return this.medicaments.filter(medicament => medicament.getNom() === nom)[0]
    }

    rechercherVaccin(nom: string) {
        return this.vaccins.filter(vaccin => vaccin.getNom() === nom)[0]
    }

    recupererListeVaccins() {
        for (let i in this.vaccins) {
            // console.log(this.vaccins[i].getNom())
            console.log("Liste des vaccins: ", this.vaccins.map(() => this.vaccins[i]))
        }
    }

    recupererListeClients() {
        for (let i in this.clients) {
            // console.log("Liste des clients: ", this.clients[i].getNom())
            Utils.consoleArray(this.clients.map(() => this.clients[i]))
        }
    }

    /**
     * On récupère la liste des médicaments où le stock est de 0
     */
    recupererListeMedicamentsO() {
        // console.log("Liste des clients: ", this.clients[i].getNom())
        //if (this.medicaments[i].getStock() === 0) {
        // console.log("Liste des médicaments 0: ", this.medicaments.filter((medicaments) => this.medicaments[i]))
        Utils.consoleArray(this.medicaments.filter((medicaments) => medicaments.getStock() === 0))
        // } else {

        // }

    }

    insertionListeVaccins() {
        for (let i in this.vaccins) {
            this.array.push(this.list_vaccins)
        }
    }

    reApprovisionnerStock(quantity: number){
        this.medicaments.filter(medicament => medicament.getStock() === 0).forEach(element => {
            element.augmenterStock(quantity*10);
            console.log("réapprovisionnement des médicaments:", element.getNom(), element.getStock())
        });
    }

}