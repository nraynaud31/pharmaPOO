import IAdresse from "../models/IAdresse";

/*
permet de définir des méthodes
dont l'implémentation se fait dans les classes filles. 
*/
export default abstract class Personne {
    abstract nom: string;
    abstract adresse: IAdresse | undefined;

    public abstract getNom(): string
}
