import Main from "./class/Main";

const maPharmacie = new Main();

// Clients
const client1 = maPharmacie.ajouterClient('bob', 1000, ["hépatite B"]);
const client2 = maPharmacie.ajouterClient('jay', 2000,["vaccin1", "BCG"]);
const client3 = maPharmacie.ajouterClient('Johny', 1000,["vaccin1", "BCG"]);
const client4 = maPharmacie.ajouterClient('Philippe', 2000,["vaccin1", "BCG"]);
//maPharmacie.isProtected("bob", "tuberculose");

// Liste des clients vaccinés
maPharmacie.recupererListeClients();

// Médicaments
maPharmacie.ajouterMedicament("Doliprane", 20, 54);
maPharmacie.ajouterMedicament("Dafalgan", 32, 54);
maPharmacie.ajouterMedicament("Lisopaine", 10, 54);
maPharmacie.ajouterMedicament("Lisopaine2", 10, 0);
maPharmacie.ajouterMedicament("Lisopaine3", 10, 0);

// Liste des médicaments où le stock est = 0
maPharmacie.recupererListeMedicamentsO();

// Réapprovisionner les médicaments où le stock est = 0
maPharmacie.reApprovisionnerStock(20);

// Vaccins
maPharmacie.ajouterVaccin("Vivant atténué", "BCG", 12, 54);
maPharmacie.ajouterMaladie("tuberculose", "BCG");
maPharmacie.ajouterVaccin("inactivés", "hépatite B", 12, 54);

// Recherches dans mes arrays

const client = maPharmacie.rechercherClient("bob");
const monMedoc = maPharmacie.rechercherMedicament("Dafalgan");
const monVaccin = maPharmacie.rechercherVaccin('BCG');



// Achats
client.acheterMedicament(monMedoc);
client.acheterVaccin(monVaccin)

